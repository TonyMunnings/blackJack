const suits = ["♠", "♡", "♢", "♣"];
const ranks = ["Ace", 2, 3, 4, 5, 6, 7, 8, 9, 10, "Knave", "Queen", "King"];

const deck =[];
for (const suit of suits) {
    for (const rank of ranks) {
        deck.push({
            suit,
            rank,
        });
    }
}

function getDeck() {
    return deck;
}
function getRandomCard() {
    return deck[Math.floor(Math.random() * deck.length)];

}
function dealRandomCard() {
    dealToDisplay(getRandomCard());
  }
const mapRanksToWords = { 
    2: "Two", 
    3: "Three", 
    4: "Four", 
    5: "Five", 
    6: "Six", 
    7: "Seven", 
    8: "Eight", 
    9: "Nine", 
    10: "Ten", 
};

function rankToWord(rank) {
    if (typeof rank === "number") {
        return mapRanksToWords[rank];
    } else {
        return rank;
    }
};

const mapSuitsToWords= {
    "♠": "Spades", 
    "♡": "Hearts", 
    "♢": "Diamonds", 
    "♣": "Clubs",
    "": "Mystery"
};

function suitToWord(suit) {
    return mapSuitsToWords[suit];

};

const mapRanksToValues = {
    Ace: "11/1",
    King: "10",
    Queen: "10",
    Knave: "10",
    "Face Down": "?"
};

function rankToValue(rank) {
    if (typeof rank === "number") {
    return rank.toString();
    } else {
        return mapRanksToValues[rank];
    }

};
const hitButton = document.querySelector("#hit-button");
hitButton.addEventListener("click", () => {
const randomCard =  getRandomCard();
console.log('Your card is ${randomCard.rank} of ${randomCard.suit}. Did you bust?');
console.log(randomCard);
});
